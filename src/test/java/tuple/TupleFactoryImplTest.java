package tuple;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;



import com.eclipseoptions.javatest.tuple.api.Tuple;
import com.eclipseoptions.javatest.tuple.api.TupleFactory;
import com.eclipseoptions.javatest.tuple.api.TupleFactoryImpl;

class TupleFactoryImplTest {
	
	String[] strArr = {"A","B","C","D"};
	Collection<String> collection = Arrays.asList(strArr);
	
	TupleFactory tf =  new TupleFactoryImpl();
	Tuple<String> t_arr = tf.newTuple(strArr);
	Tuple<String> t_collection = tf.newTuple(collection);
	
	@Test
	public void testSize() {
		
		int len = strArr.length;
		assertTrue(t_arr.size() == len);
		assertTrue(t_collection.size() == len);
		
	}
	
	@Test
	public void testGet() {
		
		String elem_arr = t_arr.get(2);
		String elem_coll = t_collection.get(2);
				
		assertTrue(elem_arr.equals("C"));
		assertTrue(elem_coll.equals("C"));
		
		assertFalse(elem_arr.equals("A"));
		assertFalse(elem_coll.equals("A"));
		
	}
	
	@Test
	public void testReplace() {
		
		Tuple<String> tArr = t_arr.replace(2, "Ashwin");
		Tuple<String> tColl = t_collection.replace(2, "Ashwin");
		
		assertFalse(t_arr.equals(tArr));
		assertFalse(t_collection.equals(tColl));
		
		// Revert the replace
		tArr = tArr.replace(2, "C");
		tColl = tColl.replace(2, "C");
		
		assertTrue(t_arr.equals(tArr));
		assertTrue(t_collection.equals(tColl));
		
	}
	
	@Test
	public void testToArray() {
		
		Object[] obj_t_arr = t_arr.toArray();
		Object[] obj_t_coll = t_collection.toArray();
		
		assertTrue(obj_t_arr instanceof Object[]);
		assertTrue(obj_t_coll instanceof Object[]);
		
		assertTrue(obj_t_arr.length == t_arr.size());
		assertTrue(obj_t_coll.length == t_collection.size());
		
		for(int i=0; i<t_arr.size(); i++) {
			
			assertTrue(obj_t_arr[i].equals(t_arr.get(i)));
			assertTrue(obj_t_coll[i].equals(t_collection.get(i)));
		}
		
	}
	
}
