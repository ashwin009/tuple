package com.eclipseoptions.javatest.tuple.api;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class TupleImpl<T> implements Tuple<T>{

	private final int DEFAULT_CAPACITY = 10;
	private final Object[] EMPTY_T = {};
	private final Object[] DEFAULTCAPACITY_EMPTY_T = {};
	
	protected int modCount = 0;
	 
	Object[] element_t;
	private int size;
	
	
	public TupleImpl() {
		// TODO Auto-generated constructor stub
		this.element_t = DEFAULTCAPACITY_EMPTY_T;
	}
	
	public TupleImpl(int initialCapacity) {
		// TODO Auto-generated constructor stub
		if (initialCapacity > 0) {
            this.element_t = new Object[initialCapacity];
        } else if (initialCapacity == 0) {
            this.element_t = EMPTY_T;
        } else {
            throw new IllegalArgumentException("Illegal Capacity: "+
                                               initialCapacity);
        }
	}
	
	public TupleImpl(Tuple<? extends T> tuple) {
		// TODO Auto-generated constructor stub

		element_t = tuple.toArray();
        if ((size = element_t.length) != 0) {
            if (element_t.getClass() != Object[].class)
            	element_t = Arrays.copyOf(element_t, size, Object[].class);
        } else {
            this.element_t = EMPTY_T;
        }
    
	}
	
	public void add(T elem) {
		
		element_t[size++] = elem;
	}
	
	public T get(int index) {
		// TODO Auto-generated method stub
		if(index < 0 || index > element_t.length-1) {
			throw new IndexOutOfBoundsException("Index is negative or greater than the last element index");
		}
		
		T t = (T) element_t[index];
		return t;
	}

	public Tuple<T> replace(int index, T value) {
		// TODO Auto-generated method stub
		
		if(index < 0 || index > element_t.length-1) {
			throw new IndexOutOfBoundsException("Index is negative or greater than the last element index");
		}
		
		Object oldValue = element_t[index];
		element_t[index] = value;
		Tuple<T> result = new TupleImpl<T>(this);
		element_t[index] = oldValue;
		
		return result;
	}

	public int size() {
		// TODO Auto-generated method stub
		return this.size;
	}

	public Object[] toArray() {
		// TODO Auto-generated method stub
		Object[] obj = new Object[this.size];
		
		for(int i=0; i<this.size; i++) {
			
			obj[i] = this.get(i);
			
		}
		
		return obj;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(DEFAULTCAPACITY_EMPTY_T);
		result = prime * result + DEFAULT_CAPACITY;
		result = prime * result + Arrays.hashCode(EMPTY_T);
		result = prime * result + Arrays.hashCode(element_t);
		result = prime * result + modCount;
		result = prime * result + size;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		
		boolean result = false;
		
		Tuple<T> t = (Tuple<T>) obj;
		
		if(t.size() == this.size) {
			for(int i=0; i<t.size(); i++) {
				if(t.get(i) == this.get(i)) {
					result = true;
				}else {
					result = false;
					break;
				}
			}
		}
		
		return result;
	}
	
	

	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Itr();
	}
	
	
	private class Itr implements Iterator<T> {
		

        int cursor;       // index of next element to return
        int lastRet = -1; // index of last element returned; -1 if no such

        Itr() {}

        public boolean hasNext() {
            return cursor != size;
        }

        @SuppressWarnings("unchecked")
        public T next() {
            int i = cursor;
            if (i >= size)
                throw new NoSuchElementException();
            Object[] elementData = TupleImpl.this.element_t;
            if (i >= elementData.length)
                throw new ConcurrentModificationException();
            cursor = i + 1;
            return (T) elementData[lastRet = i];
        }

	}
}
