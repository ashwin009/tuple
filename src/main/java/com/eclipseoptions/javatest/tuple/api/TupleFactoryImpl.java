package com.eclipseoptions.javatest.tuple.api;

import java.util.Collection;

public class TupleFactoryImpl implements TupleFactory{

	public <T> Tuple<T> newTuple(T... arr) {
		// TODO Auto-generated method stub
		
		if(arr == null) {
			throw new NullPointerException("Array is Null");
		}
		
		int len = arr.length;
		TupleImpl<T> t = new TupleImpl<T>(len);
		
		if(len == 0) {
			System.out.println("Array is Empty. Returning empty Tuple");
			return t;
		}
		
		for(int i=0; i<len; i++) {
			t.add(arr[i]);
		}
		
		return t;
	}

	public <T> Tuple<T> newTuple(Collection<T> collection) {
		// TODO Auto-generated method stub
		
		if(collection == null) {
			throw new NullPointerException("Collection is Null");
		}
		
		int size = collection.size();
		
		TupleImpl<T> t = new TupleImpl<T>(size);
		
		if(collection.isEmpty()) {
			System.out.println("Collection is Empty. Returning empty Tuple");
			return t;
		}
		
		for(T c: collection) {
			
			t.add(c);
			
		}
		
		return t;
	}

}
