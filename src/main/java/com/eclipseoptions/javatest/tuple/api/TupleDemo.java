package com.eclipseoptions.javatest.tuple.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class TupleDemo {
	
	public static void main(String[] args) {
		
		testNewTupleUsingArray();
		
		testNewTupleUsingCollection();
		
		
		
	}

	private static void testNewTupleUsingCollection() {
		// TODO Auto-generated method stub
		
		System.out.println("Testing Using Collection");
		
		Collection<String> collection = new ArrayList<String>();
		collection.add("A");
		collection.add("B");
		collection.add("C");
		collection.add("D");
		collection.add("E");
		
		Tuple<String> t = new TupleImpl<String>();
		TupleFactory tf =  new TupleFactoryImpl();
		t = tf.newTuple(collection);
		
		for(int i=0; i<t.size(); i++) {
			System.out.print(t.get(i)+" ");
		}
		
		System.out.println();
	}

	private static void testNewTupleUsingArray() {
		
		System.out.println("Testing Using Array");
		
		String[] strArr = {"A","B","C","D"};
		
		Tuple<String> t = new TupleImpl<String>();
		TupleFactory tf =  new TupleFactoryImpl();
		t = tf.newTuple(strArr);
		
		for(int i=0; i<t.size(); i++) {
			System.out.print(t.get(i)+" ");
		}
		System.out.println();
		
		// Tesing replace
		t.replace(2, "Ashwin");
		
		for(int i=0; i<t.size(); i++) {
			System.out.print(t.get(i)+" ");
		}
		System.out.println();
		
		t.replace(2, "C"); 	// Reverting 
		
		// Testing Size
		
		System.out.println(t.size());
		
		// Testing toArray
		
		Object[] obj = t.toArray();
		
		for(Object o: obj) {
			System.out.print(o+" ");
		}
		
		System.out.println();
		
		// Testing Iterator
		
		Iterator<String> itr = t.iterator();
		while(itr.hasNext()) {
			System.out.print(itr.next()+" ");
		}
		System.out.println();
		
		// Testing Equals
		
		Tuple<String> t2 = tf.newTuple(strArr);
		System.out.println("t & t2 are: " + t.equals(t2));
		
		String[] strArr2 = {"A","C","B","D","E"};
		Tuple<String> t3 = tf.newTuple(strArr2);
		System.out.println("t & t3 are: " + t.equals(t3));
		
	}

}
